//
//  MapRouteViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/18/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapRouteViewController: UIViewController  ,CLLocationManagerDelegate {
    
    //MARK:- Declaration of var and objects
    var locationManager:CLLocationManager!
    var current_latitude = String()
    var current_longitude = String()
    var desti_longitude = Double()
    var startCurrentLoc = Bool()
    var desti_latitude = Double()

     @IBOutlet weak var mapView : GMSMapView!

    @IBOutlet weak var passenger_dropLoc: UILabel!
     @IBOutlet weak var btn_start: UIButton!

    
    @IBOutlet weak var passenger_distance: UILabel!
    @IBOutlet weak var passenger_mob: UILabel!
    @IBOutlet weak var passenger_name: UILabel!
    var userInfo = [String:AnyObject]()
   var timer = Timer()
   
    @IBAction func btn_back(_ sender: Any) {
       _ =  self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print(coordinate.latitude)
        print(coordinate.longitude)
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print(marker.position.latitude)
        print(marker.position.longitude)
        return true
    }
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.backItem?.title = ""
        
        // self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        
        passenger_name.text = (userInfo["FirstName"])! as? String
        passenger_dropLoc.text = (userInfo["drop_location"])! as? String
        passenger_distance.text = "Distance - " + (userInfo["distance"]! as! String)
        passenger_mob.text = userInfo["Mobile"]! as? String
       // passenger_long.text = userInfo["PassengerLng"]! as? String
        
       desti_latitude = Double(((userInfo["PassengerLat"])! as? String)!)!
         desti_longitude = Double(((userInfo["PassengerLng"])! as? String)!)!
      
        // call determine loction method
        self.determineCurrentLocation()
        startCurrentLoc = false
    }
    
    
    //MARK:- determine Current Location for user
    func determineCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        //        if CLLocationManager.locationServicesEnabled() {
        //            //locationManager.startUpdatingHeading()
        //            locationManager.startUpdatingLocation()
        //        }
    }
    
    
    
    //MARK:- Core Location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // let userLocation:CLLocation = locations[0] as CLLocation
        guard let userLocation = locations.first else { return }
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        //manager.stopUpdatingLocation()
        
        current_latitude = String(userLocation.coordinate.latitude)
        current_longitude = String(userLocation.coordinate.longitude)
        
        if startCurrentLoc == false
        {
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude,longitude:  userLocation.coordinate.longitude, zoom: 14)
       // mapView = GMSMapView.map(withFrame:CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height-70), camera: camera)
         mapView.camera = camera
        mapView.isMyLocationEnabled = true
        self.view.addSubview(mapView) //= mapView
        
        // TODO:- get formatted address of current user usimng geo location
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            
            // Print each key-value pair in a new row
            addressDict.forEach { print($0) }
            
            // Print fully formatted address
            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
                print(formattedAddress.joined(separator: ", "))
                let userAddress = formattedAddress.joined(separator: ", ")
                
                let marker = GMSMarker()
                marker.position = center
                marker.title = (addressDict["Name"] as? String)!
                marker.snippet = userAddress
                marker.map = self.mapView
                
            }
            // TODO:- showing location of other user
            let position = CLLocationCoordinate2D(latitude:self.desti_latitude, longitude: self.desti_longitude)
            let marker = GMSMarker(position: position)
            marker.title = "Hello World"
            marker.map = self.mapView
            marker.icon = UIImage(named:"vender_icon")
            
            // TODO:- call function to draw route from source to destination using google direction api
            self.drawPathFromSourcetoDestination()
            
            //            // Access each element manually
            //            if let locationName = addressDict["Name"] as? String {
            //                print(locationName)
            //            }
            //            // Access each element manually
            //            if let locationstreet = addressDict["Street"] as? String {
            //                print(locationstreet)
            //            }
            //            if let street = addressDict["Thoroughfare"] as? String {
            //                print(street)
            //            }
            //            if let city = addressDict["City"] as? String {
            //                print(city)
            //            }
            //            if let zip = addressDict["ZIP"] as? String {
            //                print(zip)
            //            }
            //            if let country = addressDict["Country"] as? String {
            //                print(country)
            //            }
            
        })
        }
        else
        {
            
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
    //MARK:- server call function to draw route from source to destination using google direction api
    func drawPathFromSourcetoDestination ()
    {
        
        let para:NSMutableDictionary = NSMutableDictionary()
        
        let origin = "\(current_latitude),\(current_longitude)"
        let destination = "\(desti_latitude),\(desti_longitude)"
        //create the url with NSURL
        let urlValue = NSURL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving")
        // print(urlValue ?? default value)
        //create the session obje
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: urlValue! as URL)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: para, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as? [String:Any] {
                    print(json)
                    
                    
                    // TODO:- showing polyline for route in google map
                    let routes = (json["routes"] as! Array<Dictionary<String, AnyObject>>)[0]
                    let  routeOverviewPolyline = routes["overview_polyline"] as! Dictionary<String, AnyObject>
                    let points = routeOverviewPolyline["points"]
                    
                    
                    DispatchQueue.main.async {
                        let path = GMSPath.init(fromEncodedPath: points! as! String)
                        let polyline = GMSPolyline.init(path: path)
                        polyline.map = self.mapView
                        // polyline.strokeColor = UIColor .black
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    
    // called every time interval from the timer
    func timerAction()
    {
        print("timer")
        self.determineCurrentLocation()
        self.callupdateRideMethod()
    }
    
    
    @IBAction func btn_startRide(_ sender: Any)
    {
         startCurrentLoc = true
        
      
       
        if  btn_start.titleLabel?.text == "START"
        {
            self.determineCurrentLocation()
            self.callstartRideMethod()
            btn_start.setTitle("END", for: .normal)
            
        }
       else
        {
            timer.invalidate()
            self.callendRideMethod()
            btn_start.setTitle("START", for: .normal)
        }
    }
    
    
    //MARK:-  server call of login method
    func callstartRideMethod()
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "lat": current_latitude,
            "lng": current_longitude,
            "booking_id": userInfo["booking_id"] as! String,
            "status": "start",
            "DriverID": UserDefaults.standard.object(forKey:
                "DriverID")!
        ]
        print(parameters)
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetGetRideStatusUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
                 DispatchQueue.main.async {
                self.timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
                // call alert controller in main queue
               
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
        
    }

    func callupdateRideMethod()
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "lat": current_latitude,
            "lng": current_longitude,
            "booking_id": userInfo["booking_id"] as! String,
            "status": "update",
            "DriverID": UserDefaults.standard.object(forKey:
                "DriverID")!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetGetRideStatusUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
                
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
        
    }
    //MARK:-  server call of login method
    func callendRideMethod()
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "lat": current_latitude,
            "lng": current_longitude,
            "booking_id": userInfo["booking_id"] as! String,
            "status": "end",
            "DriverID": UserDefaults.standard.object(forKey:
                "DriverID")!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetGetRideStatusUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            

            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            let data = result["data"]as (AnyObject)
             let msg = data["amount"] as! NSNumber
            let amount =    "Amount - " + String(describing: msg) + " $"
            if success == "true"
            {
             
               
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:message , message: amount)
                }
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
