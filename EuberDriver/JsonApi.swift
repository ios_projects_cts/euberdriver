//
//  JsonApi.swift
//  YouTow
//
//  Created by chawtech solutions on 1/31/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class JsonApi: NSObject {

    //MARK:- server call method definition
    func callUrlSession(urlValue:String,para:(AnyObject),isSuccess:Bool, withCompletionHandler:@escaping (_ result:(AnyObject)) -> Void)
        
    
    {      //create the session object
      //  let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url:NSURL(string:urlValue) as! URL)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: para, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task =  URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(dataString)
            
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
           
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("api-",json)

                    if(isSuccess){
                        withCompletionHandler(json as (AnyObject))
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
                // post a notification
                NotificationCenter.default.post(name: NSNotification.Name(rawValue:"serverfailure"), object: nil, userInfo:nil)
                
//                let alertController = UIAlertController(title: "alert", message: "didFinishLaunchingWithOptions", preferredStyle: .alert)
//                let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//                        alertController.addAction(OKAction)
//                self.present(alertController, animated: true, completion: nil)
            }
            
        })
        
        task.resume()
        
}
}
