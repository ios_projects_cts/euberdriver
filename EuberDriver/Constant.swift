//
//  Constant.swift
//  YouTow
//
//  Created by chawtech solutions on 1/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import SystemConfiguration

//MARK:- server url
var kbaseUrl = "http://35.167.21.236/euberAPI/"
var kadditionalLoginUrl = "driver/login"
var kadditionalRegisterUrl = "registration"
var kadditionalgetLocationUpdateUrl =  "driver/update_location"
var kadditionalacceptUserrequestUrl =  "driver/response_request"
var kadditionalgetGetHistoryUrl = "driver/getHistory"
var kadditionalLogoutUrl = "driver/logout"
var kadditionalgetGetRideStatusUrl = "driver/ride_status"
var kadditionalupdateProfileUrl = "driver/editprofile"
var kadditionalgetProfileUrl = "driver/getProfile"
var kadditionalupdateFcmTokensUrl = "driver/updateFCM"
//var kadditionalneedServiceUrl = "customer/need_service"
//var kadditionalneedTowUrl = "customer/need_tow"

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

extension UIViewController {
    
    //MARK:- set alert controller globally
    func pushNotifAlertWithMessage(viewController: UIViewController, title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title:"Accept", style: .default, handler: nil))
         ac.addAction(UIAlertAction(title:"Decline", style: .default, handler: nil))
        viewController.present(ac, animated: true)
    }
}
extension UIViewController {
    
    func backViewController(vc:UIViewController, animated:Bool)
    {
        self.navigationController?.pushViewController(vc,animated:animated)
    }
}




extension UIViewController {
    
    //MARK:- set alert controller globally
    func displayAlertWithMessage(viewController: UIViewController, title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
        viewController.present(ac, animated: true)
    }
}
class Constant: NSObject {

    
    
}
