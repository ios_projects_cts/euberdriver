//
//  AppDelegate.swift
//  EuberDriver
//
//  Created by chawtech solutions on 2/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import UserNotificationsUI
import UserNotifications
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SWRevealViewControllerDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate {

    var window: UIWindow?
    var notifBack: Bool!
     var revealViewController :SWRevealViewController!
   var acceptStatus = String()
     var UserInfo = [AnyHashable: Any]()
    

    func handlepushnotificationAppDelegate(_ notification: NSNotification) {
    
     let userInfo = notification.userInfo?["data"] as (AnyObject)
        
    let pickloc = userInfo["pick_location"]as! String
    
    let dropLoc = userInfo["drop_location"] as! String
    
    let str = "PICKUP LOCATION - \n \n " + pickloc + "\n \n " + "DROP LOCATION - \n  \n" + dropLoc
    
        
        // let data = userInfo["data"]
        let alert = UIAlertController(title: "Ride Detail \n", message: str, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Accept", style: .default) { action in
            self.acceptStatus = "1"
            self.callacceptuserRequestMethod(userInfo: userInfo as! [String : AnyObject])
        })
        alert.addAction(UIAlertAction(title: "Decline", style: .default) { action in
            // perhaps use action.title here
            self.acceptStatus = "0"
            self.callacceptuserRequestMethod(userInfo: userInfo as! [String : AnyObject])
        })
        
        self.window?.rootViewController?.present(alert, animated: true)
        
    }
    
    //MARK:-  server call of login method
    func callacceptuserRequestMethod(userInfo:[String:AnyObject])
    {
        
        let apiCall = JsonApi()
        let parameters = [
            
            "PassengerID": (userInfo["PassengerID"]),
            "booking_id": (userInfo["booking_id"]),
            "DriverID": UserDefaults.standard.object(forKey:"DriverID")!,
            "status": acceptStatus
            
        ]
        
//        let alert = UIAlertController(title: "Alert", message:String(describing: parameters), preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
//        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//        
//        
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalacceptUserrequestUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    // self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let vc  = storyboard.instantiateViewController(withIdentifier:"MapRouteViewController") as! MapRouteViewController
                    vc.userInfo = userInfo
                    
                    let navC = UINavigationController(rootViewController: vc)
                    
                    self.window?.rootViewController = navC
                    self.window?.makeKeyAndVisible()

                   // self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                let alert = UIAlertController(title: "Message", message:message, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
                            
                }
            }
            
        }
        
        
    }

    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
       
//           // print("app recieved notification from remote\(notification)")
//           // self.application(application, didReceiveRemoteNotification: notification!)
//        }
//        else {
//            print("app did not recieve notification")
//        }
        
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 129/255.0, green: 180/255.0, blue: 65/255.0, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]

        
        //TODO:- call next view controller
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let viewcontroller = storyboard.instantiateViewController(withIdentifier: "ViewController")
        let viewControllerObj = MenuViewController()
        let navC = UINavigationController(rootViewController: viewcontroller)
        revealViewController = SWRevealViewController(rearViewController: viewControllerObj,frontViewController:navC)
        revealViewController.bounceBackOnOverdraw = true
        revealViewController.delegate = self
        self.window?.rootViewController = revealViewController
        self.window?.makeKeyAndVisible()

        
        //TODO:- registered key for google map and google places api
        GMSServices.provideAPIKey("AIzaSyBBW7GnkE4XVKxEq5w87FwDx1-EHuZPM1g")
        GMSPlacesClient.provideAPIKey("AIzaSyBBW7GnkE4XVKxEq5w87FwDx1-EHuZPM1g")
        
        
        
        //  Mark:- FCM Registration
        FIRApp.configure()
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        NotificationCenter.default.addObserver(self, selector:
            #selector(tokenRefreshNotification), name:
            NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        application.registerForRemoteNotifications()
        
        let token = FIRInstanceID.instanceID().token()
        print(token)
        
        
        
        
        if launchOptions != nil {
            //opened from a push notification when the app is closed
            let userInfo: [AnyHashable: Any]? = (launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as?  [AnyHashable: Any])
            if userInfo != nil {
               // print("userInfo->\(userInfo?["aps"] as? String)")
                
                notifBack = true
               UserInfo = userInfo!
                
//                let alert = UIAlertController(title: "Alert", message:String(describing: userInfo), preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
//                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
               // self.handleTerminatedAppNotification(userInfo: userInfo!)
                
               // let DataDict:[String: AnyObject] = ["data": userInfo as AnyObject]
                
                // post a notification
              //  NotificationCenter.default.post(name: NSNotification.Name(rawValue:"notificationFire"), object: nil, userInfo: DataDict)
            }
        }
        else
        {
            //opened app without a push notification.
        }

        
        return true
    }
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage)
    {
        
    }
    
    
    // MARK:- token Refresh Notification method for FCM
    func tokenRefreshNotification(_ notification: Notification) {
         let refreshedToken = FIRInstanceID.instanceID().token() 
//            print("InstanceID token: \(refreshedToken)")
//            UserDefaults.standard.set(refreshedToken, forKey:"fcm_token")
//        }
        print(refreshedToken)
        if (refreshedToken != nil)
        {
            
            let driverId = UserDefaults.standard.object(forKey:"DriverID")as? String
            if driverId != nil {
                
                if refreshedToken != UserDefaults.standard.object(forKey:"fcm_token") as? String
                {
                    if Reachability.isConnectedToNetwork() == true {
                        print("Internet connection OK")
                        let userinfo: [String:AnyObject] = ["refreshtoken": refreshedToken! as AnyObject]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "REGISTERFCMTOKEN"), object:userinfo)
                    } else {
                        print("Internet connection FAILED")
                        
                    }
                }
            }
            else
            {
                UserDefaults.standard.set(refreshedToken, forKey:"fcm_token")
                UserDefaults.standard.synchronize()
            }
        }

        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // MARK:- connectToFcm method for FCM
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // Let FCM know about the message for analytics etc.
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        // handle your message
    }
    
    // MARK:- didRegisterForRemoteNotificationsWithDeviceToken
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    
    
    //MARK:- didReceiveRemoteNotification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //   if let messageID = userInfo[gcmMessageIDKey] {
        //      print("Message ID: \(messageID)")
        //  }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //  if let messageID = userInfo[gcmMessageIDKey] {
        //      print("Message ID: \(messageID)")
        //  }
        
        // Print full message.
        print(userInfo)
        
        
        let DataDict:[String: AnyObject] = ["data": userInfo as AnyObject]
//        if application.applicationState == UIApplicationState.inactive || application.applicationState == UIApplicationState.background {
//            //opened from a push notification when the app was on background
//            // post a notification
//          
//            
//            // call alert controller in main queue
//            DispatchQueue.main.sync {
        
                
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue:"notificationFire"), object: nil, userInfo: DataDict)
           // }

        //}
        // post a notification
      //  NotificationCenter.default.post(name: NSNotification.Name(rawValue:"notificationFire"), object: nil, userInfo: DataDict)
        
        //Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlepushnotificationAppDelegate(_:)), name: NSNotification.Name(rawValue: "notificationFire"), object: nil)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    

    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

