//
//  ViewController.swift
//  EuberDriver
//
//  Created by chawtech solutions on 2/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    override var prefersStatusBarHidden: Bool{
        if UIApplication.shared.statusBarFrame.size.height == 20 {
            print("Status bar is 20 so returning NO for hidden")
            return false
        }
        print("Status bar is not 20 so returning YES for hidden")
        return true
    }
    
    
//    override var prefersStatusBarHidden: Bool {
//        return true
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // TODO:- set navigation controller properties
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = false
        
        // TODO:- wait splash for 2 min after that call next controller
        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            
            
            let email = UserDefaults.standard.object(forKey:"email")as? String
            
            if email == nil
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
        }
        
        
     //Register to receive notification in your class
     //   NotificationCenter.default.addObserver(self, selector: #selector(self.handlepushnotification(_:)), name: NSNotification.Name(rawValue: "notificationFire"), object: nil)
    }
    
    // handle notification
 /*   func handlepushnotification(_ notification: NSNotification) {
        
        let data = notification.userInfo?["data"]
        print(data)
        
        let alertController = UIAlertController(title: "alert", message: "didReceive", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
        
        //        let alertController = UIAlertController(title: "alert", message: "didFinishLaunchingWithOptions", preferredStyle: .alert)
        //        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        //        alertController.addAction(OKAction)
        //        self.present(alertController, animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"MapRouteViewController") as! MapRouteViewController
        vc.userInfo = data as! [String : AnyObject]
      //  self.navigationController?.pushViewController(vc, animated: false)
        self.present(vc, animated: true, completion: nil)
        let alert = UIAlertController(title: "Message", message: "Passenger request", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Accept", style: .default) { action in
           // self.acceptStatus = "1"
            
           // self.callacceptuserRequestMethod(userInfo: data as! [String : AnyObject])
        })
        alert.addAction(UIAlertAction(title: "Decline", style: .default) { action in
            // perhaps use action.title here
          //  self.acceptStatus = "0"
           // self.callacceptuserRequestMethod(userInfo: data as! [String : AnyObject])
        })
        
        self.present(alert, animated: true)
        
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

