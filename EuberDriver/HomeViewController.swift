//
//  HomeViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/13/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//


import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

// MARK:- Autocomplete delegate methods of google place api
extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // TODO: Handle the user's selection.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
        
       // lbl_pickupAdd.text = place.name + place.formattedAddress!
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
        // TODO:User canceled the operation.
      //  if lbl_pickupAdd.text == "" {
       //     lbl_pickupAdd.text = self.userAddress
        //}
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

class HomeViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {
    
    //MARK:- declaration var objects and outlets
 
    var locationManager:CLLocationManager!
    var userAddress = String()
    var current_latitude = String()
    var current_longitude = String()
    var acceptStatus = String()
    var placeMark = CLPlacemark()
    @IBOutlet weak var mapView : GMSMapView!
    @IBOutlet weak var menuButton : UIButton!
      let marker = GMSMarker()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
   
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        

        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.view.backgroundColor = UIColor.white
        // self.navigationController?.navigationBar.barTintColor = UIColor.clear
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.fireMenuSubChild), name: NSNotification.Name(rawValue:"gotosubchild"), object: nil)
        
        //Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlepushnotification(_:)), name: NSNotification.Name(rawValue: "notificationFire"), object: nil)
        
        NotificationCenter.default.addObserver(
            self,selector: #selector(registerFcmTokentoServer),
            name:NSNotification.Name(rawValue: "REGISTERFCMTOKEN"),object: nil)
    }
    
    func customLeftButtonAction() {
        self.revealViewController().revealToggle(animated: true);
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        OperationQueue.main.addOperation {() -> Void in
            
            print(coordinate.latitude)
            print(coordinate.longitude)
//            if myMarker {
//                myMarker.map = nil
//                myMarker = nil
//            }
//            myMarker = GMSMarker()
//            myMarker.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
//            myMarker.title = "title"
//            myMarker.map = mapView_
        }
    }
    
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        // set notification center remove observber
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"gotosubchild"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"notificationFire"), object: nil)
        
        NotificationCenter.default.removeObserver(
            self,
            name:NSNotification.Name(rawValue:"REGISTERFCMTOKEN"),object: nil)
        
        
    }

    func registerFcmTokentoServer(notification: NSNotification)
    {
        if let userInfo = notification.object as? [String:AnyObject] {
            let refreshToken = userInfo["refreshtoken"] as? String
            if refreshToken != nil {
                callupdateFcmTokenToServerMethod(refreshFcmToken:refreshToken!)
            }
            
        }
    }
    
    //MARK:-  server call of callupdateFcmTokenToServer Method
    func callupdateFcmTokenToServerMethod(refreshFcmToken:String)
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "DriverID": UserDefaults.standard.object(forKey:
                "DriverID")!,
            "fcm_key": refreshFcmToken,
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalupdateFcmTokensUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
        
    }

    // handle notification
    func handlepushnotification(_ notification: NSNotification) {
        
        let data = notification.userInfo?["data"] as (AnyObject)
        print(data)
        
        
//        let alertController = UIAlertController(title: "alert", message: "didReceive", preferredStyle: .alert)
//        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(OKAction)
//       self.present(alertController, animated: true, completion: nil)
////        let alertController = UIAlertController(title: "alert", message: "didFinishLaunchingWithOptions", preferredStyle: .alert)
////        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
////        alertController.addAction(OKAction)
////        self.present(alertController, animated: true, completion: nil)
        
        
//        let alert1 = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
//        alert1.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
//        self.present(alert1, animated: true, completion: nil)
    
        
        
        let pickloc = data["pick_location"]as! String
        
        let dropLoc = data["drop_location"] as! String
        
          let str = "PICKUP LOCATION - \n \n " + pickloc + "\n \n " + "DROP LOCATION - \n  \n" + dropLoc

        
        let alert = UIAlertController(title: "Ride Detail \n", message: str, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Accept", style: .default) { action in
            self.acceptStatus = "1"
            
           self.callacceptuserRequestMethod(userInfo: data as! [String : AnyObject])
        })
        alert.addAction(UIAlertAction(title: "Decline", style: .default) { action in
            // perhaps use action.title here
             self.acceptStatus = "0"
            self.callacceptuserRequestMethod(userInfo: data as! [String : AnyObject])
        })
        
        self.present(alert, animated: true)
        
    }
    
    //MARK:-  server call of login method
    func callacceptuserRequestMethod(userInfo:[String:AnyObject])
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "PassengerID": (userInfo["PassengerID"]!),
            "booking_id": (userInfo["booking_id"]!),
            "DriverID": UserDefaults.standard.object(forKey:"DriverID")!,
            "status": acceptStatus
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalacceptUserrequestUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                   // self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let vc  = storyboard.instantiateViewController(withIdentifier:"MapRouteViewController") as! MapRouteViewController
                    vc.userInfo = userInfo
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
        
    }

    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let notifBack = appDelegate.notifBack
        if notifBack == true
        {
            let data = appDelegate.UserInfo
            
            let pickloc = data["pick_location"]as! String
            
            let dropLoc = data["drop_location"] as! String
            
            let str = "PICKUP LOCATION - \n \n " + pickloc + "\n \n " + "DROP LOCATION - \n  \n" + dropLoc
            
            let alert = UIAlertController(title: "Ride Detail \n", message: str, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Accept", style: .default) { action in
                self.acceptStatus = "1"
                
                self.callacceptuserRequestMethod(userInfo: data as! [String : AnyObject])
            })
            alert.addAction(UIAlertAction(title: "Decline", style: .default) { action in
                // perhaps use action.title here
                self.acceptStatus = "0"
                self.callacceptuserRequestMethod(userInfo: data as! [String : AnyObject])
            })
            
            self.present(alert, animated: true)
          
        }

//        //MARK:- set effect of uiview for search
//        
//        self.view_searchBack.backgroundColor = UIColor.white
//        self.view_searchBack.layer.cornerRadius = 3.0
//        self.view_searchBack.layer.borderWidth = 2.0
//        self.view_searchBack.layer.borderColor = UIColor.clear.cgColor
//        self.view_searchBack.layer.shadowColor = UIColor.lightGray.cgColor
//        self.view_searchBack.layer.shadowOpacity = 1.0
//        self.view_searchBack.layer.shadowRadius = 1.0
//        self.view_searchBack.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(3))
        mapView.delegate = self
        
        if Reachability.isConnectedToNetwork() == true
        {
            print("Internet Connection Available!")
            self.determineCurrentLocation()
           
        }
        else
        {
            print("Internet Connection not Available!")
            DispatchQueue.main.async {
                self.displayAlertWithMessage(viewController: self, title:"Alert! \n ", message:"Please check your network connection")
            }
        }

        
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                self.determineCurrentLocation()
            }
        } else {
            print("Location services are not enabled")
            self.displayAlertWithMessage(viewController: self, title: "Message", message: "Location services are not enabled \n please check your settings")
        }
        

        
    }
    
  
    
    @IBAction func btn_menu(_ sender: Any)
    {
        if revealViewController() != nil
        {
            let reveal: SWRevealViewController? = self.revealViewController()
            reveal?.revealToggle(self)
            
         //   self.revealViewController().revealToggle(animated: true)
        }
       
    }
    
//    @IBAction func btn_menuAction(_ sender: Any)
//    {
//        
//        //        let mainVC = MenuViewController(nibName:"MenuViewController", bundle:nil)
//        //        mainVC.view.frame = CGRect(x:0,y:0, width:150, height:568)
//        //    //    self.navigationController.pushViewController(mainVC, true);
//        //        let animation = CATransition()
//        //        animation.duration = 0.5
//        //        animation.type = kCATransitionPush
//        //        animation.subtype = kCATransitionFromRight
//        //        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        //        mainVC.view.layer.add(animation, forKey: "SwitchToView")
//        //
//        //        self.present(mainVC, animated: true, completion: nil)
//        let reveal: SWRevealViewController? = self.revealViewController()
//        reveal?.revealToggle(self)
//        
//        //        let controller:ForgetViewController = self.storyboard!.instantiateViewController(withIdentifier: "ForgetViewController") as! ForgetViewController
//        //      //  controller.ANYPROPERTY=THEVALUE // If you want to pass value
//        //        controller.view.frame = CGRect(x:0,y:0, width:150, height:568)
//        //        controller.willMove(toParentViewController: self)
//        //        self.view.addSubview(controller.view)
//        //        self.addChildViewController(controller)
//        //        //controller.didMove(toParentViewController: self)
//    }
//
    
    // MARK:- handle side menu button notification
    func fireMenuSubChild(_ notification: NSNotification) {
        
        if notification.userInfo?["name"]as! String == "home"
        {
            self.revealViewController().revealToggle(self)
        }
        if notification.userInfo?["name"]as! String == "payment"
        {
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is PaymentViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"PaymentViewController") as! PaymentViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)
                
            }
        }
        else  if notification.userInfo?["name"]as! String == "history"
        {
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is HistoryViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"HistoryViewController") as! HistoryViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)
                
            }
            
        }
        if notification.userInfo?["name"]as! String == "notifications"
            
        {
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is NotificationsViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"NotificationsViewController") as! NotificationsViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            
            
            
        }
        if notification.userInfo?["name"]as! String == "settings"
            
        {
            
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is SettingsViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"SettingsViewController") as! SettingsViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            
            
        }
        if notification.userInfo?["name"]as! String == "help"
            
        {
            
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is HelpViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"HelpViewController")
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            
            
        }
        
        if notification.userInfo?["name"]as! String == "profile"
            
        {
            
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is ProfileViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"ProfileViewController")
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            
            
        }

        if notification.userInfo?["name"]as! String == "logout"
            
        {
            
            let apiCall = JsonApi()
            let parameters = [
                
                "DriverID": UserDefaults.standard.object(forKey: "DriverID")!
                
            ]
            
            apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalLogoutUrl, para: parameters as (AnyObject), isSuccess: true)
            { (result) -> Void in
                
                print("service-",result)
                
                let status = result["status"] as! String
                if status == "success"
                {
                    let success = result["success"] as! String
                    let message = result["message"] as! String
                    
                    print(status)
                    if success == "true"
                    {
                        self.callLogoutSuccessMethod()
                    }
                    else
                    {
                        // display alert controller
                        self.displayAlertWithMessage(viewController:self, title: message, message:"")
                    }
                }
                else
                {
                    
                }
                
            }
        }
    }
    
    
    func callLogoutSuccessMethod() {
        // remove objects from key value in user defaults
        //        if let bundle = Bundle.main.bundleIdentifier {
        //            UserDefaults.standard.removePersistentDomain(forName: bundle)
        //        }
        
        UserDefaults.standard.removeObject(forKey:"name")
        UserDefaults.standard.removeObject(forKey:"email")
        UserDefaults.standard.removeObject(forKey:"mobile")
        UserDefaults.standard.removeObject(forKey:"DriverID")
        
        
        // call next view controller in main queue
        DispatchQueue.main.async {
            
            
            let viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is LoginViewController) {
                    
                    controller .removeFromParentViewController()
                    // viewControllerAlreadyPushed = true
                    
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                _ =  self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)
            }
            
        }
        
    }

    
    // MARK:- btn_logcation Action
    @IBAction func btn_locationAction(_ sender: Any)
    {
        
        self.determineCurrentLocation()
    }
    //MARK:- determine Current Location for user
    func determineCurrentLocation()
    {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
//                if CLLocationManager.locationServicesEnabled() {
//                    //locationManager.startUpdatingHeading()
//                    locationManager.startUpdatingLocation()
//                }
    }
    
    //MARK:- location manager delagate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         let userLocation:CLLocation = locations[0] as CLLocation
       // guard let userLocation = locations.first else { return }
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        locationManager.stopUpdatingLocation()
        
        current_latitude = String(userLocation.coordinate.latitude)
        current_longitude = String(userLocation.coordinate.longitude)
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude,
        longitude:userLocation.coordinate.longitude, zoom: 16)
        // mapView = GMSMapView.map(withFrame:CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height-40), camera: camera)
        mapView.camera = camera
        mapView.isMyLocationEnabled = false
        //  self.view.addSubview(mapView)
        self.callupdateDriverLocationMethod()
        
        // get firmatted address of current user from geolocation
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            print(addressDict)
             //Print each key-value pair in a new row
               addressDict.forEach { print($0) }
            
            // Print fully formatted address
            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
                print(formattedAddress.joined(separator: ", "))
                self.userAddress = formattedAddress.joined(separator: ", ")
                
            }
//            
//                        // Access each element manually
//                        if let locationName = addressDict["Name"] as? String {
//                            print(locationName)
//                        }
//                        // Access each element manually
//                        if let locationstreet = addressDict["Street"] as? String {
//                            print(locationstreet)
//                        }
//                        if let street = addressDict["Thoroughfare"] as? String {
//                            print(street)
//                        }
//                        if let city = addressDict["City"] as? String {
//                            print(city)
//                        }
//                        if let zip = addressDict["ZIP"] as? String {
//                            print(zip)
//                        }
//                        if let country = addressDict["Country"] as? String {
//                            print(country)
//                        }
//            if let locality = addressDict["Locality"] as? String {
//                print(locality)
//            }

            
            
          
            self.marker.position = center
            self.marker.title = (addressDict["Name"] as? String)!
            self.marker.snippet = self.userAddress
            self.marker.map = self.mapView
            
        })
        
        
//        var geocoder = CLGeocoder()
//        var location1 = CLLocation(latitude: 28.618324, longitude: 77.372595)
//        geocoder.reverseGeocodeLocation(location1) {
//            (placemarks, error) -> Void in
//            if let placemark1 = placemarks , (placemarks?.count)! > 0 {
//                let placemark = placemark1[0]
//                print(placemark.addressDictionary)
//            }
//        
//        } 
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
    //MARK:-  server call of login method
    func callupdateDriverLocationMethod()
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "lat": current_latitude,
            "lng": current_longitude,
            "DriverID": UserDefaults.standard.object(forKey: "DriverID")!
          
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetLocationUpdateUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
 
    //MARK:- btn_bookride Action
//    @IBAction func btn_bookrideAction(_ sender: Any) {
//        
//        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//        let vc  = storyboard.instantiateViewController(withIdentifier: "BookQuoteViewController") as! BookQuoteViewController
//        self.navigationController?.pushViewController(vc, animated: false)
//    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
